```
aws cloudformation create-stack 
    --stack-name TestStack
    --parameters ParameterKey=InstanceType,ParameterValue=t2.micro
    --template-body file://week0.yml 
```